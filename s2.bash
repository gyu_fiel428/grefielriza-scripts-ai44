#!/bin/bash
read -p  "Please enter your last name : " lname
read -p  "Please enter your first name : " fname
read -p  "Please enter birthday (YYYY-MM-DD) : " bday
MAX=$(printf '%s' $(( $(date -u -d"$(date +"%Y-%m-%d")" +%s) - $(date -u -d"$bday" +%s))))
echo "Hello, ${lname^} ${fname^}! You're $(((((($MAX/60)/60)/24)/364))) years old"